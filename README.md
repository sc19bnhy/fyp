# Modelling How Brains Control Behaviour in Worms

This project combines two existing models to improve our understanding on C. elegans. <br/> 
The project integrates (Izquierdo & Beer, 2018) and (Denham et al., 2018) into one model.



## Libraries Needed
```
sudo apt install g++
sudo apt install git
sudo apt install cmake
sudo apt install paraview
sudo apt install libatlas-base-dev
sudo apt install libsuitesparse-dev
sudo apt install python-is-python3
sudo apt install python3-pip
pip install numpy
pip install scipy
```

## Project Installation
```
git clone git@gitlab.com:sc19bnhy/fyp.git FYP
cd FYP/project/new-model
mkdir build && cd build
cmake -D CMAKE_BUILD_TYPE=Release ..
make
ctest
```

## ParaView Guide
<pre>
Choose <b>File</b> then Choose <b>Open</b>
Navigate to <b>FYP/project/new-model/build</b>
Choose <b>vtu</b> files and then <b>Apply</b>
Change the colouring method from <b>W</b> to <b>u</b>
Click on <b>-Z</b> once to adjust perspective
Choose <b>View</b> and enable <b>Animation View</b>
Click on <b>worm_0*</b> and choose <b>Camera</b>
Click on <b>Orbit</b> and choose <b>Follow Fata</b>
Add the configuration by clicking <b>+</b>
</pre>



## Acknowledgment
The project uses and builds upon: <br/>
(Izquierdo & Beer, 2018) : https://github.com/edizquierdo/RoyalSociety2018 <br/>
(Denham et al., 2018) : https://bitbucket.org/leedswormlab/curve-worm-royal-society-paper/src/master/ <br/>
Interpolation Library from ttk592 : https://github.com/ttk592/spline/blob/master/src/spline.h
