#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <vector>
#include "spline.h"
#include <cstdio>

using namespace std;


int main()
{
    int time_counter = 1000;

    std::vector<double> x(12, 0);
    std::vector<double> d(12, 0);
    std::vector<double> v(12, 0);
    std::vector<double> new_d(128, 0);
    std::vector<double> new_v(128, 0);

    for (int i = 0; i < 12; i++)
    {
      x[i] = i / 11.0;
    }

    std::string line;
    std::ifstream file("izq_mac/act.dat");

    for (int i = 0; i < time_counter; i++)
    {
      getline(file, line);
    }
    file.close();

    int i_d = 0;
    int i_v = 0;
    int counter = 0;

    std::stringstream ss(line);
    std::string word;

    while (ss >> word)
    {
      double neu_act = atof(word.c_str());
      if (counter > 84)
      {
        if ((counter % 2) != 0)
        {
          d[i_d] = neu_act;
          i_d++;
        }
        else
        {
          v[i_v] = neu_act;
          i_v++;
        }
      }
      counter++;
    }

    tk::spline s_d(x, d);
    tk::spline s_v(x, v);

    double X;
    for (int i = 0; i < 128; i++)
    {
      X = i / 127.0;
      new_d[i] = s_d(X);
      new_v[i] = s_v(X);
    }

    for (int i = 0; i < 128; i++)
    {
        cout << "dorsal:  " << new_d[i] << endl;
        cout << "ventral: " << new_v[i] << endl;
    }
}