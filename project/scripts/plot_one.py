import os
import os.path

from pathlib import Path
from matplotlib import pyplot as plt

x = []
y = []
k = []
time = 0

cwd = os.getcwd()
FileData = Path(cwd+"/../new-model/build/Kappa.txt")

with open(FileData, 'r') as data:
    for line in data.readlines():
        try:
            line = line.split()[1::]
            for num, val in enumerate(line):
                x.append(time)
                y.append(num / 127.0)
                k.append(float(val))
            time+=0.005
        except:
            val = 0
    outputData = data.read()

plt.title('Curvature Kymogram')
plt.xlabel('time')
plt.ylabel('position')
plt.xlim(0, 6)
plt.ylim(0, 1)
plt.grid()
plt.scatter(x, y, s=10, c=k)
k_bar = plt.colorbar()
k_bar.set_label(label="k")
plt.clim(-10, 10)
plt.show()