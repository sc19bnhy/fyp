import os
import shutil
import os.path

from pathlib import Path
from matplotlib import pyplot as plt

loop = False
while loop == False:

    cwd = os.getcwd()
    option = input("\nchoose 'a' to add data\nchoose 'd' to show data \nchoose 's' to stop program\n")

    if option == 'a':
        print("\nCurrent files in the dir:")
        for file in os.listdir(cwd+"/Kappa"):
            if file.endswith("_k.txt"):
                print(file)

        FileNaming = False

        while FileNaming == False:
            print("\nEnter new file name:")
            FileName = input()
            kFileName   = Path(cwd+"/Kappa/"+FileName+"_k.txt")
            actFileName = Path(cwd+"/Activations/"+FileName+"_act.txt")
            if not kFileName.is_file():
                FileNaming = True
            if FileNaming == False:
                print("\nFile already exists!")

        actNewFile = open(actFileName, 'w+')
        shutil.copyfile(cwd+"/../new-model/build/Activation.txt", cwd+"/Activations/"+FileName+"_act.txt")
        actNewFile.close()

        kNewFile = open(kFileName, 'w+')
        shutil.copyfile(cwd+"/../new-model/build/Kappa.txt", cwd+"/Kappa/"+FileName+"_k.txt")
        kNewFile.close()

        print("Files added!")

    if option == 'd':
        condition = False

        print("\nCurrent files:")
        for file in os.listdir(cwd+"/Kappa"):
            if file.endswith("_k.txt"):
                print(file)
        print("\nWhich data to plot? enter the first name before '_' of the k file?")

        while condition == False:
            FileData = Path(cwd+"/Kappa/"+input()+"_k.txt")
            if FileData.is_file():
                condition = True
            if condition == False:
                print("\nFile doesn't exist!")

        x = []
        y = []
        k = []
        time = 0

        with open(FileData, 'r') as data:
            for line in data.readlines():
                try:
                    line = line.split()[1::]
                    for num, val in enumerate(line):
                        x.append(time)
                        y.append(num / 127.0)
                        k.append(float(val))
                    time+=0.005
                except:
                    val = 0
            outputData = data.read()

        plt.title('Curvature Kymogram')
        plt.xlabel('time')
        plt.ylabel('position')
        plt.xlim(0, 6)
        plt.ylim(0, 1)
        plt.grid()
        plt.scatter(x, y, s=10, c=k)
        k_bar = plt.colorbar()
        k_bar.set_label(label="k")
        plt.clim(-10, 10)
        plt.show()

    if option == 's':
        break
        