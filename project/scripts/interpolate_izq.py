import os
import math
import matplotlib
import numpy as np

from math import log10, floor
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d


x_t_data = []
y_d_data = []
y_v_data = []
time_counter = 0

readFile = "izq_mac/act.dat"
with open(readFile, 'r') as readData:
    for line in readData.readlines():
        time_counter+=1
        try:
            line = line.split()[1::]
            for num, val in enumerate(line):
                if time_counter == 750:
                    if num > 83 and (num % 2) == 0:
                        y_d_data.append(float(val))
                    if num > 83 and (num % 2) != 0:
                        y_v_data.append(float(val))
        except:
            val = 0
    for i in range(12):
        x_t_data.append(i/11.0)
    y_d_f = interp1d(x_t_data, y_d_data, kind='cubic', fill_value='extrapolate')
    y_v_f = interp1d(x_t_data, y_v_data, kind='cubic', fill_value='extrapolate')
    x = np.linspace(0.0, 1.0, 1000)
    dtemp = y_d_f(x)
    vtemp = y_v_f(x)
    y_d = y_d_f(x)
    y_v = y_v_f(x)
    plt.scatter(x, y_d, color='r')
    plt.scatter(x, y_v, color='b')
    plt.show()
    x_t_data.clear()
    y_d_data.clear()
    y_v_data.clear()
    plt.show()
